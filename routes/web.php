<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/lti', [App\Http\Controllers\LTIToolController::class, 'ltiMessage']);
Route::get('/lti/jwks', [App\Http\Controllers\LTIToolController::class, 'getJWKS']);
Route::get('/eportal_response_edit', [App\Http\Controllers\LTIToolController::class, 'eportal_response_edit_index']);

Route::get('/lti/setplatform', [App\Http\Controllers\LTIToolController::class, 'setPlatfromInfo']);