<?php

namespace App\Constants;

class Constants
{

    const MSG_ERROR = [
        //事前連携情報には存在しないエラー
        'platform_id_not_match' => 'Platform not found or no platform authentication request URL.',
        'test_id_not_exist' => 'test_id not found.',
        //Initiate login parameter error
        'login_hint_error' => 'Missing login_hint parameter.',
        'target_link_uri_error' => 'Missing target_link_uri parameter.',
        'create_session_error' => 'Unable to generate a state value.',
    ];
}
