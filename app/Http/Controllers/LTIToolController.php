<?php

namespace App\Http\Controllers;

use ceLTIc\LTI;
use Illuminate\Http\Request;
use App\Services\LtiToolService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use longhornopen\LaravelCelticLTI\LtiTool;

class LTIToolController extends Controller
{
    private LtiToolService $ltiToolService;
    
    public function __construct(LtiToolService $ltiToolService)
    {
        $this->ltiToolService = $ltiToolService;
    }

    /**
     * 学習eポータル向けテストの入口
     *
     * @param Request $request
     * @return void
     */
    public function initLogin(Request $request)
    {
        $initLoginResult = $this->ltiToolService->initLoginRequestChecker($request);
        if(isset($initLoginResult['valid'])){
            return View::make('lti.eportal_response_edit', ['result' => $initLoginResult]);
        }elseif(isset($initLoginResult['result_final'])){
            return View::make('lti.eportal_result', ['result' => $initLoginResult]);
        }elseif(isset($initLoginResult['data'])){
            return response()->json($initLoginResult);
        }else{
            return $initLoginResult;
        }

    }   

    /**
     * getJWKS
     *
     * @return void
     */
    public function getJWKS() {
        $tool = LtiTool::getLtiTool();
        return $tool->getJWKS();
    }

    /**
     * setPlatfromInfo
     *
     * @return void
     */
    public function setPlatfromInfo() {
        $pdo = DB::connection()->getPdo();
        $dataConnector = LTI\DataConnector\DataConnector::getDataConnector($pdo, '', 'pdo');
        $platform = LTI\Platform::fromPlatformId('https://saltire.lti.app/platform', 'saltire.lti.app', 'cLWwj9cbmkSrCNsckEFBmA', $dataConnector);
        $platform->name = 'Test saLTIre';
        $platform->authorizationServerId = null;  // defaults to the Access Token URL
        $platform->authenticationUrl = 'https://saltire.lti.app/platform/auth';
        $platform->accessTokenUrl = 'https://saltire.lti.app/platform/token/sb5d78c24f6449e787afa11d2b4c76a19n';
        $platform->rsaKey = null;  // a public key is not required if a JKU is available
        $platform->jku = 'https://saltire.lti.app/platform/jwks';
        $platform->signatureMethod = 'RS256';
        $platform->enabled = true;
        $platform->save();
        return "Set Platform success to table lti2_consumer";
    }

}
