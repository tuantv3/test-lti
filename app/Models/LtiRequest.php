<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class LtiRequest extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */

    protected $table = 'lti_requests';

    protected $fillable = ['request_id','test_id','from','to','method','request_type','message_type','request_header','request_body','result_json'];

}
