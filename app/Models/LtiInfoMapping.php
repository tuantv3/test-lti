<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class LtiInfoMapping extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */

    protected $table = 'lti_info_mapping';

}
