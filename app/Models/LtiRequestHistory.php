<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class LtiRequestHistory extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */

    protected $table = 'lti_requests_history';

    protected $fillable = ['request_id','test_id','comformant','business_comment','business_comment_updated_at','center_comment','center_comment_updated_at','center_comment_by'];

}
