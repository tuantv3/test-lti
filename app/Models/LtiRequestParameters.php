<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class LtiRequestParameters extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */

    protected $table = 'lti_requests_parameters';

    protected $fillable = ['request_id','params','request_type','message_type'];
}
