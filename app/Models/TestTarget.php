<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class TestTarget extends Model
{
    use HasFactory;
    use Notifiable;

    protected $table = 'test_targets';
    public $primaryKey = 'test_id';
    protected $fillable = ['test_target_login_id', 'type', 'initial_password', 'password', 'business_name', 'note', 'access_token', 'refresh_token'];
    protected $guarded = ['test_id', 'created_at', 'updated_at'];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }
}
