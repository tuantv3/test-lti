<?php

namespace App\Services;

use ceLTIc\LTI\System;
use ceLTIc\LTI\Platform;
use LonghornOpen\LaravelCelticLTI\LtiTool;
use ceLTIc\LTI\Tool;
use ceLTIc\LTI\Util;
use ceLTIc\LTI\OAuth;
use ceLTIc\LTI\PlatformNonce;

class ToolService
{
    use System;

    private $tool;

    /**
     * Toolのインスタンス作成
     * LonghornOpen\LaravelCelticLTI\LtiToolのPackage経由
     */
    public function __construct()
    {
        $tool = LtiTool::getLtiTool();
        $this->tool = $tool;
    }
    
    /**
     * カスタマイズしないメソッド
     */
    public function onInitiateLogin($requestParameters, &$authParameters)
    {
        $hasSession = !empty(session_id());
        if (!$hasSession) {
            session_start();
        }
        $_SESSION['ceLTIc_lti_authentication_request'] = array(
            'state' => $authParameters['state'],
            'nonce' => $authParameters['nonce']
        );
        if (!$hasSession) {
            session_write_close();
        }
    }

    /**
     * カスタマイズしたメソッド
     */
    public function getMessageParameters($strictMode = false, $disableCookieCheck = false, $generateWarnings = false)
    {
            $this->parseMessage($strictMode, $disableCookieCheck, $generateWarnings);
            return $this->messageParameters;
    }

    /**
     * カスタマイズしたメソッド
     */
    public function sendAuthenticationRequest($param,$disableCookieCheck = false) 
    {
        $platform = Platform::fromPlatformId($param['iss'], $param['client_id'], $param['lti_deployment_id'], $this->tool->dataConnector);
        
        $oauthRequest = OAuth\OAuthRequest::from_request();
        $usePlatformStorage = !empty($oauthRequest->get_parameter('lti_storage_target'));
        $session_id = '';
        if ($usePlatformStorage) {
            $usePlatformStorage = empty($_COOKIE[session_name()]) || ($_COOKIE[session_name()] !== session_id());
        }
        if (!$disableCookieCheck) {
            if (empty(session_id())) {
                if (empty($_COOKIE)) {
                    Util::setTestCookie();
                }
            } elseif (empty($_COOKIE[session_name()]) || ($_COOKIE[session_name()] !== session_id())) {
                $session_id = '.' . session_id();
                if (empty($_COOKIE[session_name()])) {
                    Util::setTestCookie();
                }
            }
        }
        do {
            $state = Util::getRandomString();
            $nonce = new PlatformNonce($platform, "{$state}{$session_id}");
            $ok = !$nonce->load();
        } while (!$ok);
        $nonce->expires = time() + Tool::$stateLife;
        $ok = $nonce->save();
        if ($ok) {
            $redirectUri = $oauthRequest->get_normalized_http_url();
            $requestNonce = Util::getRandomString(32);
            $params = array(
                'client_id' => $platform->clientId,
                'login_hint' => $param['login_hint'],
                'nonce' => $requestNonce,
                'prompt' => 'none',
                'redirect_uri' => $redirectUri,
                'response_mode' => 'form_post',
                'response_type' => 'id_token',
                'scope' => 'openid',
                'state' => $nonce->getValue()
            );
            if (isset($param['lti_message_hint'])) {
                $params['lti_message_hint'] = $param['lti_message_hint'];
            }
            $this->onInitiateLogin($param, $params);
            return $params;
        } else {
            return null;
        }
    }
}