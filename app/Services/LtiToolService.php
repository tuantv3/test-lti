<?php

namespace App\Services;

use Exception;
use ceLTIc\LTI\Util;
use App\Models\LtiRequest;
use App\Constants\Constants;
use App\Models\Lti2Consumer;
use App\Models\LtiInfoMapping;
use App\Models\LtiRequestHistory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Models\LtiRequestParameters;

class LtiToolService
{
    private array $data_insert_tbl_request;
    private array $data;
    private $systemModel;

    /**
     * ToolServiceインスタンス作成
     */
    public function __construct()
    {
        $systemModel = new ToolService();
        $this->systemModel = $systemModel;
    }

    /**
     * 学習eポータル向けテスト_InitLoginリクエスト処理
     * 学習eポータル向けテスト_ResourceLinkRequest
     * @param [type] $request
     * @return void
     */
    public function initLoginRequestChecker($request) 
    {
        $request_param = $request->all();

        //リクエストパラメーターにリクエストIDが無い場合
        if(!isset($request_param['request_id'])){
            if(isset($request_param['iss'])){
                //InitLoginリクエストのパラメーターを取得
                $parameters = Util::getRequestParameters();
                //リクエストの送信元を取得
                $from_url = $parameters['iss'];
                //事前連携確認
                $checker = $this->jizenRenkeiKakunin($from_url);
                if (isset($checker['request_id']) && isset($checker['test_id'])){
                    //DB保存
                    $this->insertRequestParameter($checker['request_id'],$parameters);
                    $this->insertRequestHistory($checker['request_id'],$checker['test_id']);

                    $this->data_insert_tbl_request['request_id'] = $checker['request_id'];
                    $this->data_insert_tbl_request['test_id'] = $checker['test_id'];
                    // InitLoginリクエスト検証（LTI-PHP）
                    if (isset($parameters['iss']) && (strlen($parameters['iss']) > 0)) {  // Initiate login request
                        if (!isset($parameters['login_hint']) || (strlen($parameters['login_hint']) <= 0)) {
                            $this->data['valid'] = false;
                            $this->data['error'] = ['login_hint' => Constants::MSG_ERROR['login_hint_error']];
                        } elseif (!isset($parameters['target_link_uri']) || (strlen($parameters['target_link_uri']) <= 0)) {
                            $this->data['valid'] = false;
                            $this->data['error'] = ['target_link_uri' => Constants::MSG_ERROR['target_link_uri_error']];
                        } else {
                            $this->data['valid'] = true;
                            // 検証結果をDB保存
                            $this->data_insert_tbl_request['request_type'] = 'InitLogin';
                            $this->data_insert_tbl_request['message_type'] = 'request';
                            $this->insertRequest($this->data_insert_tbl_request,$this->data);
                        }
                    }else{
                        $this->insertRequest($this->data_insert_tbl_request,null);
                    }
                    // 認証リクエスト生成（LTI-PHP）
                    $oauthRequest = $this->systemModel->sendAuthenticationRequest($parameters);
                    if(empty($oauthRequest)){
                        return $this->resultJson([],Constants::MSG_ERROR['create_session_error']);
                    }
                    // 認証リクエストのパラメーター,検証結果、リクエストIDをレスポンスとして「認証リクエスト再入力画面」に返す（view）
                    $this->data['oauthRequest'] = $oauthRequest;
                    $this->data['parameters'] = $parameters;
                    $this->data['request_id'] = $checker['request_id'];
                    return $this->data;
                }else{
                    return $checker;
                }
                //----------------------------------------------------
            }else{
                // ResourceLinkRequestの解析
                $resource_link_req_data = $this->systemModel->getMessageParameters();
                ksort($resource_link_req_data);
                $platform_id = $resource_link_req_data['platform_id'];
                // 事前連携確認
                $checker = $this->jizenRenkeiKakunin($platform_id);
                if (isset($checker['request_id']) && isset($checker['test_id'])){
                    $this->data_insert_tbl_request['request_id'] = $checker['request_id'];
                    $this->data_insert_tbl_request['test_id'] = $checker['test_id'];
                    $this->insertRequestParameter($checker['request_id'],$request_param);
                }else{
                    return $checker;
                }
                // ツールインスタンスでResourceLinkRequest検証（LTI-PHP）
                // 未実装
                // 検証結果をDB保存
                $resource_link_req_checker = false;  
                $result_json = [];
                if($resource_link_req_checker){
                    $this->data_insert_tbl_request['request_type'] = 'ResourceLink';
                    $this->data_insert_tbl_request['message_type'] = 'request';
                    $this->insertRequest($this->data_insert_tbl_request,$result_json);
                }else{
                    $this->insertRequest($this->data_insert_tbl_request,null);
                }
                //検証結果画面に検証結果を返す
                $result_final['result_final'] = $resource_link_req_data;
                $result_final['request_id'] = $checker['request_id'];
                return $result_final;
            }
        //リクエストパラメーターにリクエストIDがある場合
        }else{
            $this->data_insert_tbl_request['request_id'] = $request_param['request_id'];
            $test_id = '1';  //DBから取得実装必要です。
            $this->data_insert_tbl_request['test_id'] = $test_id;
            $this->data_insert_tbl_request['request_type'] = 'InitLogin';
            $this->data_insert_tbl_request['message_type'] = 'response';
            $param = [
                "client_id" => $request_param['client_id'],
                "login_hint" => $request_param['login_hint'],
                "nonce" => $request_param['nonce'],
                "prompt" => $request_param['prompt'],
                "redirect_uri" => $request_param['redirect_uri'],
                "response_mode" => $request_param['response_mode'],
                "response_type" => $request_param['response_type'],
                "scope" => $request_param['scope'],
                "state" => $request_param['state'],
                "lti_message_hint" => $request_param['lti_message_hint'],
            ];
            $authentication_request_url = 'https://saltire.lti.app/platform/auth'; //DBから取得実装必要です。
            //DB保存
            $this->insertRequest($this->data_insert_tbl_request,$param);
            $this->insertRequestParameter($request_param['request_id'],$param);
            // ツールインスタンスで認証リクエストを送信（Auto-submmitted POST Form）（LTI-PHP）
            $sendForm = Util::sendForm($authentication_request_url, $param, '', '');
            return $sendForm;

        }
    }

    /**
     * リクエストID作成
     *
     * @param [type] $test_id
     * @return void
     */
    public function createRequestId($test_id) 
    {
        $timestamp = time();
        $request_id = $test_id . '_'. $timestamp;
        return $request_id;
    }

    /**
     * Json形式結果
     *
     * @param [type] $data
     * @param [type] $msg
     * @return void
     */
    public function resultJson($data,$msg) 
    {
        return [
            'message' => $msg,
            'data' => $data,
        ];
    }

    /**
     * lti_request_parameters テーブルにInsertメソッド
     *
     * @param [type] $request_id
     * @param [type] $param
     * @return void
     */
    protected function insertRequestParameter($request_id,$param){
        try {
            DB::beginTransaction();
            LtiRequestParameters::create([
                'request_id' => $request_id,
                'request_type' => '',
                'message_type' => '',
                'params' => json_encode($param),
            ]);
            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollBack();
            Log::debug($e->getMessage());
            throw $e;
        }
    }

    /**
     * lti_request_history テーブルにInsertメソッド
     *
     * @param [type] $request_id
     * @param [type] $test_id
     * @return void
     */
    protected function insertRequestHistory($request_id,$test_id){
        try {
            DB::beginTransaction();
            LtiRequestHistory::create([
                'request_id' => $request_id,
                'comformant' => false,
                'business_comment' => '',
                'business_comment_updated_at' => date('Y-m-d H:i:s'),
                'center_comment' => '',
                'center_comment_updated_at' => date('Y-m-d H:i:s'),
                'center_comment_by' => '',
                'test_id' => $test_id,
            ]);
            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollBack();
            Log::debug($e->getMessage());
            throw $e;
        }
    }

    /**
     * lti_request テーブルにInsertメソッド
     *
     * @param [type] $data_insert_tbl_request
     * @param [type] $result
     * @return void
     */
    protected function insertRequest($data_insert_tbl_request,$result){
        try {
            DB::beginTransaction();
            LtiRequest::create([
                'test_id' => $data_insert_tbl_request['test_id'],
                'request_id' => $data_insert_tbl_request['request_id'],
                'from' => '',
                'to' => '',
                'method' => '',
                'request_type' => $data_insert_tbl_request['request_type'] ?? '',
                'message_type' => $data_insert_tbl_request['message_type'] ?? '',
                'request_header' => '',
                'request_body' => '',
                'result_json' => json_encode($result),
            ]);
            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollBack();
            Log::debug($e->getMessage());
            throw $e;
        }
    }

    /**
     * 事前連携確認メソッド
     *
     * @param [type] $from_url
     * @return void
     */
    public function jizenRenkeiKakunin($from_url){
        $hasPlatformId = Lti2Consumer::where('platform_id', $from_url)->first();
        if(!is_null($hasPlatformId)){
            $lti_infomapping = LtiInfoMapping::where('consumer_pk', $hasPlatformId['consumer_pk'])->first();
            if(!is_null($lti_infomapping)){
                $request_id = $this->createRequestId($lti_infomapping['test_id']);
                return array(
                    'request_id' => $request_id,
                    'test_id' => $lti_infomapping['test_id']);
            //test_idは存在しない場合
            }else{
                return $this->resultJson([],Constants::MSG_ERROR['test_id_not_exist']);
            }
        //Platformが事前連携情報には存在しない場合
        }else{
            return $this->resultJson([],Constants::MSG_ERROR['platform_id_not_match']);
        }
    }


}
