<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lti_info_mapping', function (Blueprint $table) {
            $table->bigIncrements('id')->unique();
            $table->bigInteger('test_id');
            $table->bigInteger('consumer_pk');
            $table->bigInteger('tool_pk');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lti_info_mapping');
    }
};
