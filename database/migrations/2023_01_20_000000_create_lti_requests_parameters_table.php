<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lti_requests_parameters', function (Blueprint $table) {
            $table->bigIncrements('id')->unique();
            $table->string('request_id');
            $table->string('request_type');
            $table->string('message_type');
            $table->json('params')->nullable();
            // $table->string('request_type');
            // $table->string('message_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lti_requests_parameters');
    }
};
