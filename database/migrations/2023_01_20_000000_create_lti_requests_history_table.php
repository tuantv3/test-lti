<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lti_requests_history', function (Blueprint $table) {
            $table->bigIncrements('id')->unique();
            $table->string('request_id');
            $table->bigInteger('test_id');
            $table->boolean('comformant');
            $table->string('business_comment');
            $table->date('business_comment_updated_at');
            $table->string('center_comment');
            $table->date('center_comment_updated_at');
            $table->string('center_comment_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lti_requests_history');
    }
};
