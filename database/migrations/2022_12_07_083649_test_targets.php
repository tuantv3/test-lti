<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('test_targets', function (Blueprint $table) {
            $table->increments('test_id')->comment("テストID");
            $table->string('test_target_login_id')->comment("テスト対象ログインID");
            $table->string('type')->comment("テスト対象種別");
            $table->string('initial_password')->comment("テスト対象初期パスワード");
            $table->string('password')->nullable()->comment("テスト対象パスワード");
            $table->boolean('password_changed')->nullable()->comment("パスワード変更済み");
            $table->string('business_name')->nullable()->comment("事業者名");
            $table->string('note')->nullable()->comment("備考");
            $table->text('access_token')->nullable()->comment("アクセストークン");
            $table->text('refresh_token')->nullable()->comment("リフレッシュトークン");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
