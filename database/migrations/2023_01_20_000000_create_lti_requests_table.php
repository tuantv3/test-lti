<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lti_requests', function (Blueprint $table) {
            $table->bigIncrements('id')->unique();
            $table->string('request_id');
            $table->bigInteger('test_id');
            $table->string('from');
            $table->string('to');
            $table->string('method');
            $table->string('request_type');
            $table->string('message_type');
            $table->string('request_header');
            $table->string('request_body');
            $table->json('result_json')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lti_requests');
    }
};
