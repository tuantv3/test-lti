<?php

namespace Database\Seeders;

use App\Models\Staff;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class TestTargetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('test_targets')->insert([
            'test_target_login_id' => 'test0001',
            'type' => 'ORC_SS',
            'initial_password' => 'Test1234',
            'password' => Hash::make('Test1234'),
            'business_name' => 'テスト事業者_OneRoster校務支援システム'
        ]);
        DB::table('test_targets')->insert([
            'test_target_login_id' => 'test0002',
            'type' => 'ORC_LE',
            'initial_password' => 'Test1234',
            'password' => Hash::make('Test1234'),
            'business_name' => 'テスト事業者_OneRoster学習eポータル'
        ]);
        DB::table('lti_info_mapping')->insert([
            'test_id' => '1',
            'consumer_pk' => '3',
            'tool_pk' => '1',
        ]);
    }
}
