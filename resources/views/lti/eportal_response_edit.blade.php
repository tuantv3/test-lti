<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href={{asset('css/bootstrap.min.css')}}>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <title>LTI/OneRosterCSV テストコンソール</title>
  </head>
  <body>
    <div class="container">
      <div class="accordion" id="accordionPanelsStayOpenExample">
        <div class="accordion-item">
          <h2 class="accordion-header" id="panelsStayOpen-headingOne">
            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseOne" aria-expanded="true" aria-controls="panelsStayOpen-collapseOne" style="background-color:rgba(0,0,0,.03);">
              Init Login リクエストパラメーター認証結果
            </button>
          </h2>
          <div id="panelsStayOpen-collapseOne" class="accordion-collapse" aria-labelledby="panelsStayOpen-headingOne">
            <div class="accordion-body">
            <!-- Init Login リクエストパラメーター認証結果 -->
              <table class="table">
                <thead>
                  <tr>
                    <th scope="col">パラメーター名</th>
                    <th scope="col">検証結果</th>
                    <th scope="col">備考</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($result['parameters'] as $key => $value)
                    <tr>
                      <td>{{ $key }}</td>
                      @if(isset($result['error']) && in_array($key,array_keys($result['error'])))
                      <td><span class="material-icons" style="color: red;">task_alt</span></td>
                      <td>{{ $result['error'][$key] }}</td>
                      @else
                      <td><span class="material-icons" style="color: #2fd42f;">task_alt</span></td>
                      <td>なし</td>
                      @endif
                    </tr>
                  @endforeach
                  </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="card clear mt-2">
        <div class="card-header pb-3">
          <div class="mt-2">
            &nbsp;OpenID Connect 認証リクエスト再入力
          </div>
        </div>
        {{ Form::open(array('route' => 'lti')) }}
        <div class="card-body">
          <div class="col clear pt-2 mt-2">
            @foreach ($result['oauthRequest'] as $key => $value)
              <div class="row clear justify-content-center">
                <label for="id_tool_consumer_instance_guid" class="col-sm-3 col-form-label">{{ $key }}:</label>
                <div class="col-sm-7">
                  <div class="input-group">
                    <input type="text" class="form-control form-control-sm recommended" name="{{$key}}" id= "{{$key}}" value="{{$value}}" placeholder="Recommended parameter">
                  </div>
                </div>
              </div>
            @endforeach
            <div class="d-flex justify-content-evenly mt-4 mb-4">
            <input type="hidden" name="request_id" id= "request_id" value="{{$result['request_id']}}">
            <input type="hidden" name="submit_type" id= "submit_type" value="insert">
              <input class="btn btn-primary" type="submit" value=" 送信 ">
            </div>
          </div>
        </div>
        {{ Form::close() }}
      </div>
    </div>
  </body>
</html>