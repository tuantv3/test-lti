<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href={{asset('css/bootstrap.min.css') }}>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <title>LTI/OneRosterCSV テストコンソール</title>
  </head>
  <body>
    <div class="container">
      <div class="card clear mt-2">
        <div class="card-header pb-3">
          <div class="mt-2 fw-bold">
            &nbsp;接続テスト検証結果
          </div>
        </div>
        <div class="card-body">
          <div class="d-flex align-items-start">
            <span class="material-icons" style="display:inline-block">
              info
            </span>
            <p class=" ms-1 fw-bold" style="display:inline-block"> 
              InitLoginリクエストと認証リクエストのパラメーター詳細については、「LTIリクエスト詳細」画面で確認してください。
            </p>
          </div>
          <table class="table">
            <thead>
              <tr style="background-color:black; color:white;">
                <th width="300px" scope="col">リクエスト情報 </th>
                <th scope="col"></th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td width="300px" scope="col">
                  <strong>
                    リクエストID: 
                  </strong>
                </td>
                <td>
                  <strong>
                  {{$result['request_id']}}
                  </strong>
                </td>
              </tr>
            </tbody>
          </table>
          <!-- accordion1 -->
          <div class="accordion" id="accordionPanelsStayOpenExample">
            <div class="accordion-item">
              <h2 class="accordion-header" id="panelsStayOpen-headingOne">
                <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseOne" aria-expanded="true" aria-controls="panelsStayOpen-collapseOne">
                  <div class="col-sm-2">Summary</div>
                </button>
              </h2>
              <div id="panelsStayOpen-collapseOne" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingOne">
                <div class="accordion-body">
                <!-- 認証結果 -->
                  <ul>
                    <li><strong>URL:</strong> {{$result['result_final']['platform_id']}}</li>
                    <li><strong>LTI version:</strong> {{$result['result_final']['lti_version']}}</li>
                    <li><strong>Type:</strong> {{$result['result_final']['lti_message_type']}}</li>
                    <li><strong>Verification:</strong> <span class="bg-success text-white ps-1 pe-1">Passed</span></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <!-- accordion2 -->
          <div class="accordion" id="accordionPanelsStayOpenExample">
            <div class="accordion-item">
              <h2 class="accordion-header" id="panelsStayOpen-heading2">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapse2" aria-expanded="true" aria-controls="panelsStayOpen-collapse2">
                  <div class="col-sm-2">Message Parameters</div>
                </button>
              </h2>
              <div id="panelsStayOpen-collapse2" class="accordion-collapse collapse" aria-labelledby="panelsStayOpen-heading2">
                <div class="accordion-body">
                <!-- 認証結果 -->
                  <pre style="white-space: pre-line;">
                  @foreach ($result['result_final'] as $key => $value)
                    {{$key}}={{$value}}
                  @endforeach
                    <!-- context_id=S3294476
                    context_label=ST101
                    context_title=Telecommunications 101
                    context_type=CourseSection
                    custom_ags_scopes=https://purl.imsglobal.org/spec/lti-ags/scope/lineitem,https://purl.imsglobal.org/spec/lti-ags/scope/lineitem.readonly,https://purl.imsglobal.org/spec/lti-ags/scope/result.readonly,https://purl.imsglobal.org/spec/lti-ags/scope/score
                    custom_assessments_url=https://saltire.lti.app/platform/extassessment/s1ab67e9703f70b9695f7378629b4a67f
                    custom_caliper_profile_url=https://saltire.lti.app/platform/caliperprofile/s1ab67e9703f70b9695f7378629b4a67f
                    custom_caliper_session_id=ZRqMAOiYbXZApV9X
                    custom_context_group_sets_url=https://saltire.lti.app/platform/groups/s1ab67e9703f70b9695f7378629b4a67f/S3294476
                    custom_context_groups_url=https://saltire.lti.app/platform/groups/s1ab67e9703f70b9695f7378629b4a67f/S3294476
                    custom_context_memberships_url=https://saltire.lti.app/platform/membership/context/s1ab67e9703f70b9695f7378629b4a67f
                    custom_context_memberships_v2_url=https://saltire.lti.app/platform/membership/context/s1ab67e9703f70b9695f7378629b4a67f
                    custom_context_setting_url=https://saltire.lti.app/platform/settings/context/s1ab67e9703f70b9695f7378629b4a67f
                    custom_gs_versions=1.0
                    custom_lineitem_url=https://saltire.lti.app/platform/gradebook/s1ab67e9703f70b9695f7378629b4a67f/S3294476/lineitems/429785226
                    custom_lineitems_url=https://saltire.lti.app/platform/gradebook/s1ab67e9703f70b9695f7378629b4a67f/S3294476/lineitems
                    custom_link_memberships_url=https://saltire.lti.app/platform/membership/link/s1ab67e9703f70b9695f7378629b4a67f
                    custom_link_setting_url=https://saltire.lti.app/platform/settings/link/s1ab67e9703f70b9695f7378629b4a67f
                    custom_nrps_versions=1.0,2.0
                    custom_oauth2_access_token_url=https://saltire.lti.app/platform/token/s1ab67e9703f70b9695f7378629b4a67f
                    custom_system_setting_url=https://saltire.lti.app/platform/settings/system/s1ab67e9703f70b9695f7378629b4a67f
                    custom_tc_profile_url=https://saltire.lti.app/platform/profile/s1ab67e9703f70b9695f7378629b4a67f
                    ext_ims_lis_basic_outcome_url=https://saltire.lti.app/platform/extoutcomes/s1ab67e9703f70b9695f7378629b4a67f
                    ext_ims_lis_memberships_id=4jflkkdf9s
                    ext_ims_lis_memberships_url=https://saltire.lti.app/platform/extmemberships/s1ab67e9703f70b9695f7378629b4a67f
                    ext_ims_lis_resultvalue_sourcedids=decimal,percentage,ratio,passfail,letteraf,letterafplus,freetext
                    ext_ims_lti_tool_setting_id=d94gjklf954kj
                    ext_ims_lti_tool_setting_url=https://saltire.lti.app/platform/extsetting/s1ab67e9703f70b9695f7378629b4a67f
                    ext_outcome_data_values_accepted=url,text -->
                  </pre>
                </div>
              </div>
            </div>
          </div>
          {{-- <!-- accordion3 -->
          <div class="accordion" id="accordionPanelsStayOpenExample">
            <div class="accordion-item">
              <h2 class="accordion-header" id="panelsStayOpen-heading3">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapse3" aria-expanded="true" aria-controls="panelsStayOpen-collapse3">
                  <div class="col-sm-6">Message Claims (derived from message parameters)</div>
                </button>
              </h2>
              <div id="panelsStayOpen-collapse3" class="accordion-collapse collapse" aria-labelledby="panelsStayOpen-heading3">
                <div class="accordion-body">
                <!-- 認証結果 -->
                  <pre style="white-space: pre-line;">aud: [
                    "jisc.ac.uk"
                    ]
                    email: jbaird@uni.ac.uk
                    family_name: Baird
                    given_name: John
                    name: John Logie Baird
                    picture: https://saltire.lti.app/images/lti.gif
                    sub: 29123
                    https://purl.imsglobal.org/spec/lti-ags/claim/endpoint/lineitem: https://saltire.lti.app/platform/gradebook/s1ab67e9703f70b9695f7378629b4a67f/S3294476/lineitems/429785226
                    https://purl.imsglobal.org/spec/lti-ags/claim/endpoint/lineitems: https://saltire.lti.app/platform/gradebook/s1ab67e9703f70b9695f7378629b4a67f/S3294476/lineitems
                    https://purl.imsglobal.org/spec/lti-ags/claim/endpoint/scope: [
                    "https://purl.imsglobal.org/spec/lti-ags/scope/lineitem",
                    "https://purl.imsglobal.org/spec/lti-ags/scope/lineitem.readonly",
                    "https://purl.imsglobal.org/spec/lti-ags/scope/result.readonly",
                    "https://purl.imsglobal.org/spec/lti-ags/scope/score"
                    ]
                    https://purl.imsglobal.org/spec/lti-bo/claim/basicoutcome/lis_outcome_service_url: https://saltire.lti.app/platform/outcomes/s1ab67e9703f70b9695f7378629b4a67f
                    https://purl.imsglobal.org/spec/lti-bo/claim/basicoutcome/lis_result_sourcedid: UzMyOTQ0NzY6Ojo0Mjk3ODUyMjY6OjoyOTEyMw==
                    https://purl.imsglobal.org/spec/lti-gs/claim/groupsservice/context_group_sets_url: https://saltire.lti.app/platform/groups/s1ab67e9703f70b9695f7378629b4a67f/S3294476
                    https://purl.imsglobal.org/spec/lti-gs/claim/groupsservice/context_groups_url: https://saltire.lti.app/platform/groups/s1ab67e9703f70b9695f7378629b4a67f/S3294476
                    https://purl.imsglobal.org/spec/lti-gs/claim/groupsservice/service_versions: [
                    "1.0"
                    ]
                    https://purl.imsglobal.org/spec/lti-nrps/claim/namesroleservice/context_memberships_url: https://saltire.lti.app/platform/membership/context/s1ab67e9703f70b9695f7378629b4a67f
                    https://purl.imsglobal.org/spec/lti-nrps/claim/namesroleservice/service_versions: [
                    "1.0",
                    "2.0"
                    ]
                    https://purl.imsglobal.org/spec/lti/claim/context/id: S3294476
                    https://purl.imsglobal.org/spec/lti/claim/context/label: ST101
                    https://purl.imsglobal.org/spec/lti/claim/context/title: Telecommunications 101
                    https://purl.imsglobal.org/spec/lti/claim/context/type: [
                    "CourseSection"
                    ]
                    https://purl.imsglobal.org/spec/lti/claim/custom/assessments_url: https://saltire.lti.app/platform/extassessment/s1ab67e9703f70b9695f7378629b4a67f
                    https://purl.imsglobal.org/spec/lti/claim/custom/caliper_profile_url: https://saltire.lti.app/platform/caliperprofile/s1ab67e9703f70b9695f7378629b4a67f
                    https://purl.imsglobal.org/spec/lti/claim/custom/caliper_session_id: ZRqMAOiYbXZApV9X
                    https://purl.imsglobal.org/spec/lti/claim/custom/context_memberships_url: https://saltire.lti.app/platform/membership/context/s1ab67e9703f70b9695f7378629b4a67f
                    https://purl.imsglobal.org/spec/lti/claim/custom/context_setting_url: https://saltire.lti.app/platform/settings/context/s1ab67e9703f70b9695f7378629b4a67f
                    https://purl.imsglobal.org/spec/lti/claim/custom/link_memberships_url: https://saltire.lti.app/platform/membership/link/s1ab67e9703f70b9695f7378629b4a67f
                    https://purl.imsglobal.org/spec/lti/claim/custom/link_setting_url: https://saltire.lti.app/platform/settings/link/s1ab67e9703f70b9695f7378629b4a67f
                    https://purl.imsglobal.org/spec/lti/claim/custom/oauth2_access_token_url: https://saltire.lti.app/platform/token/s1ab67e9703f70b9695f7378629b4a67f
                    https://purl.imsglobal.org/spec/lti/claim/custom/system_setting_url: https://saltire.lti.app/platform/settings/system/s1ab67e9703f70b9695f7378629b4a67f
                    https://purl.imsglobal.org/spec/lti/claim/custom/tc_profile_url: https://saltire.lti.app/platform/profile/s1ab67e9703f70b9695f7378629b4a67f
                    https://purl.imsglobal.org/spec/lti/claim/ext/ims_lis_basic_outcome_url: https://saltire.lti.app/platform/extoutcomes/s1ab67e9703f70b9695f7378629b4a67f
                    https://purl.imsglobal.org/spec/lti/claim/ext/ims_lis_memberships_id: 4jflkkdf9s
                    https://purl.imsglobal.org/spec/lti/claim/ext/ims_lis_memberships_url: https://saltire.lti.app/platform/extmemberships/s1ab67e9703f70b9695f7378629b4a67f
                    https://purl.imsglobal.org/spec/lti/claim/ext/ims_lis_resultvalue_sourcedids: decimal,percentage,ratio,passfail,letteraf,letterafplus,freetext
                    https://purl.imsglobal.org/spec/lti/claim/ext/ims_lti_tool_setting_id: d94gjklf954kj
                    https://purl.imsglobal.org/spec/lti/claim/ext/ims_lti_tool_setting_url: https://saltire.lti.app/platform/extsetting/s1ab67e9703f70b9695f7378629b4a67f
                    https://purl.imsglobal.org/spec/lti/claim/ext/outcome_data_values_accepted: url,text
                    https://purl.imsglobal.org/spec/lti/claim/launch_presentation/css_url: https://saltire.lti.app/css/tc.css
                    https://purl.imsglobal.org/spec/lti/claim/launch_presentation/document_target: frame
                    https://purl.imsglobal.org/spec/lti/claim/launch_presentation/locale: en-GB
                    https://purl.imsglobal.org/spec/lti/claim/launch_presentation/return_url: https://saltire.lti.app/tool
                    https://purl.imsglobal.org/spec/lti/claim/lis/course_offering_sourcedid: DD-ST101
                    https://purl.imsglobal.org/spec/lti/claim/lis/course_section_sourcedid: DD-ST101:C1
                    https://purl.imsglobal.org/spec/lti/claim/lis/person_sourcedid: sis:942a8dd9
                    https://purl.imsglobal.org/spec/lti/claim/message_type: LtiResourceLinkRequest
                    https://purl.imsglobal.org/spec/lti/claim/resource_link/description: Will ET phone home, or not; click to discover more.
                    https://purl.imsglobal.org/spec/lti/claim/resource_link/id: 429785226
                    https://purl.imsglobal.org/spec/lti/claim/resource_link/title: Phone home
                    https://purl.imsglobal.org/spec/lti/claim/roles: [
                    "urn:lti:role:ims/lis/Instructor"
                    ]
                    https://purl.imsglobal.org/spec/lti/claim/tool_platform/contact_email: vle@uni.ac.uk
                    https://purl.imsglobal.org/spec/lti/claim/tool_platform/description: A Higher Education establishment in a land far, far away.
                    https://purl.imsglobal.org/spec/lti/claim/tool_platform/guid: vle.uni.ac.uk
                    https://purl.imsglobal.org/spec/lti/claim/tool_platform/name: University of JISC
                    https://purl.imsglobal.org/spec/lti/claim/tool_platform/product_family_code: jisc
                    https://purl.imsglobal.org/spec/lti/claim/tool_platform/url: https://vle.uni.ac.uk/
                    https://purl.imsglobal.org/spec/lti/claim/tool_platform/version: X2.0
                    https://purl.imsglobal.org/spec/lti/claim/version: LTI-1p0
                  </pre>
                </div>
              </div>
            </div>
          </div> --}}
          <!-- accordion4 -->
          <div class="accordion" id="accordionPanelsStayOpenExample">
            <div class="accordion-item">
              <h2 class="accordion-header" id="panelsStayOpen-heading4">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapse4" aria-expanded="true" aria-controls="panelsStayOpen-collapse4">
                  <div class="col-sm-6">Message JWT (derived from message parameters)</div>
                </button>
              </h2>
              <div id="panelsStayOpen-collapse4" class="accordion-collapse collapse" aria-labelledby="panelsStayOpen-heading4">
                <div class="accordion-body">
                <!-- 認証結果 -->
                <pre style="white-space: pre-line;">aud: [
                  "jisc.ac.uk"
                  ]
                  email: jbaird@uni.ac.uk
                  family_name: Baird
                  given_name: John
                  name: John Logie Baird
                  picture: https://saltire.lti.app/images/lti.gif
                  sub: 29123
                  https://purl.imsglobal.org/spec/lti-ags/claim/endpoint/lineitem: https://saltire.lti.app/platform/gradebook/s1ab67e9703f70b9695f7378629b4a67f/S3294476/lineitems/429785226
                  https://purl.imsglobal.org/spec/lti-ags/claim/endpoint/lineitems: https://saltire.lti.app/platform/gradebook/s1ab67e9703f70b9695f7378629b4a67f/S3294476/lineitems
                  https://purl.imsglobal.org/spec/lti-ags/claim/endpoint/scope: [
                      "https://purl.imsglobal.org/spec/lti-ags/scope/lineitem",
                      "https://purl.imsglobal.org/spec/lti-ags/scope/lineitem.readonly",
                      "https://purl.imsglobal.org/spec/lti-ags/scope/result.readonly",
                      "https://purl.imsglobal.org/spec/lti-ags/scope/score"
                  ]
                  </pre>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>